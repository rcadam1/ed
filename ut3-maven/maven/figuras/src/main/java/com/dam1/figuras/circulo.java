package com.dam1.figuras;

/**
 * Clase instanciable para la figura círculo
 * Hello world!
 *
 */
public class circulo
{
	protected double radio;
	/**
	* Constructor con un parámetro
	@param r el radio del círculo */
	public circulo(double r) { radio=r; }
	/**
	* Método especial toString
	* @return String contenido de los atributos */
	public String toString( )
    {
    	return "Radio: " + radio;
    }
    	public double area() { return 3.14*radio*radio; }
}
